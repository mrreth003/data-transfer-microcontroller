# Data Transfer Microcontroller

EEE3097S Group Project

Hello! The Repo has been laid out to include the code used in the project in a file named "Code".

The other directories used in this repo contain files either used as part of the functioning of the program - such as "Encryption" or "Compression" - or contain
scripts that were written for testing and analysis purposes, whether it was to test the similarity of two files or the FFTs of the input and output files to determine if they 
are a match.

The demonstration video can also be found in this repo, along with the screenshot results from all the testing that was conducted thorughout the design process.

The following are commands that can be used in a linux terminal or wsl terminal to run the compression and encryption processing blocks:

Whole Process:
bzip2 -vk readSensorData.csv && openssl enc -aes-256-cbc -pbkdf2 -in readSensorData.csv.bz2 -out readSensorDataout.txt.bz2 -pass pass:ethan && rm readSensorData.csv.bz2  && openssl enc -d -aes-256-cbc -pbkdf2 -in readSensorDataout.txt.bz2 -out readSensorData.txt.bz2 -pass pass:ethan && bzip2 -d readSensorData.txt.bz2 && rm readSensorDataout.txt.bz2

Compression only
bzip2 -vk readSensorData.csv

Encryption only
openssl enc -aes-256-cbc -pbkdf2 -in readSensorData.csv.bz2 -out readSensorDataout.txt.bz2 -pass pass:ethan

Decryption only
openssl enc -d -aes-256-cbc -pbkdf2 -in readSensorDataout.txt.bz2 -out readSensorData.txt.bz2 -pass pass:ethan

Decompression only
bzip2 -d readSensorData.txt.bz2

We hope you enjoy and make effective use of these resources at hand.


Kind Regards

Ethan Morris and Ethan Meknassi